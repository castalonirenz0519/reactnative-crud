import {faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  TextInput,
  Alert,
} from 'react-native';
import {ApiCall} from '../api/api';
import {globalStyle} from '../Stylesheet';
import {FormInput} from '../Component/Form';
import {Button} from 'native-base';

export const EmployeeDetails = (props) => {
  const {details} = props.route.params;
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState({});
  const [editable, setEditable] = useState(false);

  useEffect(() => {
    props.navigation.setOptions({
      title: 'Employee Detail',
      headerShown: true,
      headerLeft: () => {
        return (
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Home')}
            style={{marginLeft: 20}}>
            <FontAwesomeIcon size={25} icon={faChevronLeft} color={'#933401'} />
          </TouchableOpacity>
        );
      },
    });

    console.log('how many times')
    getUserData();
  }, []);

  const getUserData = async () => {
    let data;
    try {
      const jsonValue = await AsyncStorage.getItem('@user_info');
      data = jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value
    }

    let objBody = {
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + data.token,
        'Content-type': 'application/json',
      },
      body: null,
      url: 'employee/' + details.id,
    };
    ApiCall(objBody).then((res) => {
      if (res.status == 200) {
        setUserData(res.data);
      } else {
        Alert.alert(
          'Warning',
          'Cannot fetch employee details, please try again.',
          [
            {
              onPress: () => {
                props.navigation.navigate('Home');
              },
            },
          ],
        );
      }
      setLoading(false);
    });
  };

  let array = [
    {id: 1, name: 'email', placeholder: 'Enter your email address.'},
    {id: 2, name: 'name', placeholder: 'Enter your name.'},
  ];

  const submit = () => {
    
    setLoading(true)
    AsyncStorage.getItem('@user_info').then((res) => {
      data = res != null ? JSON.parse(res) : null;

      let objBody = {
        method: 'put',
        headers: {
          Authorization: 'Bearer ' + data.token,
          'Content-type': 'application/json',
        },
        body: userData,
        url: 'employee/' + userData.id,
      };
      ApiCall(objBody).then((res) => {
        
        if (res.status == 200) {
           Alert.alert(
             'Success',
             'Done updating employee details',
             [
               {
                 onPress: () => {props.navigation.replace('Home')},
               },
             ],
           );
        } else {
          Alert.alert(
            'Warning',
            'Cannot update employee details, please try again.',
            [
              {
                onPress: () => {
                  setLoading(false)
                },
              },
            ],
          );
        }
      });
    });
  };

  return (
    <View style={[globalStyle.container]}>
      {!loading ? (
        <View style={{alignItems: 'center'}}>
          <FormInput
            editable={editable}
            array={array}
            email={userData.email}
            name={userData.name}
            onChangeText={(val) => setUserData({...userData, ...val})}
          />
          <View style={{marginTop: 20, flexDirection: 'row'}}>
            <Button
              onPress={() =>
                editable ? setEditable(false) : setEditable(true)
              }
              style={[
                globalStyle.button,
                {backgroundColor: editable ? 'gray' : '#933401'},
              ]}>
              <Text style={{color: '#fff'}}>Edit</Text>
            </Button>

            <Button
              onPress={submit}
              disabled={!editable ? true : false}
              style={[
                globalStyle.button,
                {
                  backgroundColor: !editable ? 'gray' : '#933401',
                  marginLeft: 20,
                },
              ]}>
              <Text style={{color: '#fff'}}>Submit</Text>
            </Button>
          </View>
        </View>
      ) : (
        <ActivityIndicator size="large" />
      )}
    </View>
  );
};
